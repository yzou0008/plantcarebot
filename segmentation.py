# from picamera import PiCamera
# from picamera.array import PiRGBArray
import numpy as np
import cv2
from matplotlib import pyplot as plt


# # initialise object
# camera = PiCamera()
# # configure camera setting
# camera.resolution = (640,480)
# camera.framerate = 32
# # initialise the picture arrage with the corresponding size
# rawCapture = PiRGBArray(camera, size=(640, 480))

kW = 11
kH = 11
blur = 0

def testVideo(file='video.mp4'):  
    # Test Video Catpure
    cap = cv2.VideoCapture(file)
    i = 1
    while(cap.isOpened()):
        ret,frame = cap.read()
        if ret == True:
            image,centers,area = combined_filter(frame)
            image = draw_circles(frame,centers,area)
            name = 'testCapture' + str(i) + '.png'
            cv2.imwrite(name,image)
            i = i+1
        else:
            break
    

def testImage(file='test.jpg'):
    # Test Image Capture
    image = cv2.imread(file)
    image,centers,area = combined_filter(image)
    image = draw_circles(image,centers,area)
    cv2.imwrite('testResult.png',image)
    return image,centers,area

def adjust_filter(lower_mask, upper_mask, level=1,direction = 1):
    adj = direction*level
    lower_mask = lower_mask + np.array([-adj,-5*adj,-2*adj])
    upper_mask = upper_mask + np.array([adj,5*adj,2*adj])

    lower_mask = np.clip(lower_mask,[0,0,0],[179,255,255])    
    upper_mask = np.clip(upper_mask,[0,0,0],[179,255,255])
    return lower_mask, upper_mask

def filter_image(image,lower_mask = np.array([100,150,150]),upper_mask = np.array([255,255,255])):
    hsv_room = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)
    
    mask = cv2.inRange(hsv_room, lower_mask,upper_mask)
    result= cv2.bitwise_and(image, image, mask=mask)
    return result

def threshold_image(image,threshold):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    ret,thresh = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    return thresh

def blob_average(image):
    # Calculate moments of binary image
    M = cv2.moments(image)

    if (M["m00"] == 0):
        M["m00"] = 0.001
    # Calculate x,y coordinates of centre
    cX = int(M["m10"] / M["m00"])
    cY = int(M["m01"] / M["m00"])
    area = M["m00"]
    # print("Coordinates",center.getX(),",",center.getY())
    return int(cX),int(cY),int(area)

def blur_image(image, blur_level = 3):
    kW = blur_level
    kH = blur_level
    return cv2.GaussianBlur(image, (kW, kH), 0)

def canny_image(image):
    image = cv2.Canny(image,100,120)
    return image

def contour_features(contours):
    centers = np.empty((0,2),int)
    area_array = np.empty(0,int)

    for i in range(len(contours)):
        if (len(contours[i]) < 3):
            cX = contours[i][0][0][0]
            cY = contours[i][0][0][1]
            area = np.array([[cX,cY]])
            centers = np.concatenate((centers,area))
            area_array = np.concatenate((area_array,[1]))

        else:
            cX,cY,area = blob_average(contours[i])
            centers = np.concatenate((centers,np.array([[cX,cY]])))
            area_array = np.concatenate((area_array,[int(cv2.contourArea(contours[i]))]))

    return centers,area_array

def contour_threshold(centers,area,threshold):
    new_centers = np.empty((0,2),int)
    new_area = np.empty(0)

    for i in range(len(centers)):
        if ((area[i]) > threshold):
            new_centers = np.concatenate((new_centers,[centers[i]]))
            new_area = np.concatenate((new_area,[area[i]]))
    return new_centers,new_area

def draw_circles(images,centers,area):
    colour = np.array([155,25,0])
    thickness = 2
    tmp_image = images
    for i in range(len(centers)):
        center_tuple = (centers[i][0],centers[i][1])
        radius = int(pow(area[i],0.5)/3.14)
        tmp_image = cv2.circle(tmp_image, center_tuple,radius,colour,thickness)
    return tmp_image

def combined_filter(image,lower_mask = np.array([100,150,150]),upper_mask = np.array([179,255,255]), expected=1):
    acceptable = False
    attempts = 0
    max_attempts = 5
    threshold = 10000
    new_lower_mask = lower_mask
    new_upper_mask = upper_mask

    while(acceptable != True):
        new_image = image
        new_image = blur_image(new_image,15)
        new_image = filter_image(new_image,new_lower_mask,new_upper_mask)
        new_image = threshold_image(new_image,200)

        im2, contours, hierarchy = cv2.findContours(new_image,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
        centers,area = contour_features(contours)
        centers,area = contour_threshold(centers,area,threshold)

        if (len(centers) < expected and attempts <= max_attempts):
            # print("Can't find points. Reducing threshold")
            threshold = threshold/1.3
            new_lower_mask,new_upper_mask = adjust_filter(new_lower_mask,new_upper_mask,1)
            attempts = attempts + 1

        elif (len(centers) > 5 and attempts <= max_attempts):
            # print("Too many points. Increasing filter")
            new_lower_mask,new_upper_mask = adjust_filter(lower_mask,upper_mask,-3)
            attempts = attempts + 1
        else:
            # print("Perfect")
            acceptable = True
            return new_image,centers,area



# # capture frames from the camera
# for frame in camera.capture_continuous(rawCapture,format="bgr",use_video_port=True):

#     # grab the raw numpy array representing the image, then initialise the timestamp and occiped/unoccupied text
#     image = frame.array    
#     rawCapture.truncate(0)
#     key = cv2.waitKey(1) & 0xF
# When everything done, release the capture
#cv2.destroyAllWindows()





