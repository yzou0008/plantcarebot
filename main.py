import cv2
import positioning
import servoMovement
import moistureReading
import camera as cameraModule
import DCMotors
import positioning
import point
import segmentation
import string
import numpy as np
import time


def watering_stage(bot):
    print("Lowering moisture sensor...")
    servoMovement.moveServo(1)
    time.sleep(0.5)
    print("Checking Moisture Status...")
    waterlevel = input("What is the moisture level? ")

    # waterlevel = moistureReading.takeMeasurement() # take value from moisture
    print("Raising moisture sensor...")
    servoMovement.moveServo(0)
    time.sleep(0.5)

    watered, status = moistureReading.pump(waterlevel)
    if (watered == True):
        print("Plant watered")
        print(status)
    else:
        print("Plant sufficiently watered already. Skipped")
        print(status)


class State:
    def __init__(self,max_plants):
        self.max_plants = max_plants
        self.watered_plants = 0

        self.shortIncrement = 0
        self.longIncrement = 0
        self.unbrokenIncrements = 0

        self.leftBound = 250
        self.rightBound = 400
        self.finished = False
        self.visitedTags = np.empty((0),int)

DCMotors.stopmotor("short")
DCMotors.stopmotor("long")

val = input("Reset the machine. Press any key when done: ")

bot = State(3)

while (bot.finished == False):
    print("Taking an image and filtering for red tags...")
    image = cameraModule.capture()
    new_image, coord, area = segmentation.combined_filter(image)

    # print("Coordinates in Image:")
    # print(coord)
    unbroken = True
    # Find all valid coordinates in the center of camera
    valid_coord = np.empty((0,2),int)

    for i in range(len(coord)):
        if (coord[i][0] >= bot.leftBound and coord[i][0] <= bot.rightBound):
            unbroken = False
            valid_coord= np.concatenate((valid_coord,np.array([coord[i]])))
    print("Valid Coords:")
    print(valid_coord)
    time.sleep(0.2)
    if (len(valid_coord) > 0):
        print("Start visiting valid points...")
        # Iterate through all valid coordinates
        valid_coord = np.sort(valid_coord,axis=1)
        # prevX = bot.shortIncrement
        for i in range(len(valid_coord)):
            # distance = (valid_coord[i][0]- prevX)/100.00
            # direction = 1
            # if (distance < 0):
            #     direction = -1
            print("Move towards point...")
            # DCMotors.motor_combined(direction,0,abs(distance),0) # Primary Move
            micro_move = positioning.move_towards_point(30) # Refined Move
            # prevX = valid_coord[i][0] # Update previous coords
            time.sleep(0.5)

            watering_stage(bot)
            bot.watered_plants = bot.watered_plants + 1
            # bot.shortIncrement = bot.shortIncrement + distance
            bot.longIncrement = bot.longIncrement
            time.sleep(0.5)
            print("Points visited...")
        
    print("Moving on...")
    #DCMotors.motor_combined(0.5,0,bot.shortIncrement,0)
    # bot.shortIncrement = 0

    # Check for Finish Conditions
    if (unbroken):
        bot.unbrokenIncrements = bot.unbrokenIncrements + 1
    else:
        bot.unbrokenIncrements = 0
    
    if (bot.unbrokenIncrements > 5 or bot.watered_plants >= bot.max_plants):
        bot.finished = True
        break

    # Move on
    DCMotors.motor_combined(0,0.5,0,1) # move right by increment
    bot.longIncrement = bot.longIncrement + 1


#DCMotors.motor_combined(0,-1,0,abs(bot.longIncrement/2)) # reset



print("Point Reached!!!")
    