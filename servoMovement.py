import time
import math
import pigpio

pi = pigpio.pi()

servo_pin = 4
frequency = 50
upperbound = 0.0025
lowerbound= 0.0005

def convertDutycycle(extension):
    if (extension > 1):
        extension = 1
    elif (extension < 0):
        extension = 0
    desired = (upperbound - lowerbound)*extension + lowerbound
    return 256*desired/(1/frequency)

def moveServo(extension, Kp=1, reset=False):
    pi.set_PWM_frequency(servo_pin,frequency)
    pi.set_PWM_dutycycle(servo_pin,convertDutycycle(0))
    pi.write(servo_pin,0)
    desired = extension
    actual = 0
    time.sleep(1)

    if (reset):
        actual = 0
        print("Resetting Servo")

        while (abs(actual - desired) > 0.1):
            actual = PIDservo(actual, desired, Kp)
            pi.set_PWM_dutycycle(servo_pin,convertDutycycle(actual))
    else:
        actual = extension
        pi.set_PWM_dutycycle(servo_pin,convertDutycycle(actual))
    time.sleep(1)
    

def PIDservo(actual, desired,Kp):
    lower_limit = 0.1
    upper_limit = 1
    
    error = desired - actual

    ut = Kp * error

    if (ut < lower_limit):
        ut = lower_limit
    elif (ut > upper_limit):
        ut = upper_limit

    time.sleep(ut*5)
    return (actual + ut)




