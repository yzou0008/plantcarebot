import pigpio
import time
import math
import numpy as np
import cv2
import RPi.GPIO as GPIO

# #Motor 1:
# OUTB = 17 #rotation count
# OUTA = 27 #direction
# 
# #Motor 2:
# OUTB = 22
# OUTA = 26
# 
# #H Bridge:
AIN1 = 17
AIN2 = 27

BIN1 = 23
BIN2 = 18

frequency = 50
timestep = 0.25

pi = pigpio.pi()

def convertToDutyCycle(speed):
    return speed*255

def motor_combined(speedA, speedB, secondsA, secondsB):
    directionA = "forward"
    directionB = "forward"

    if (speedA < 0):
        directionA = "reverse"
        speedA = abs(speedA)
    if (speedB < 0):
        directionB = "reverse"
        speedB = abs(speedB)

    motormove("short",directionA,speedA)
    motormove("long",directionB,speedB)
    if (secondsA > secondsB):
        time.sleep(secondsB)
        stopmotor("long")
        time.sleep(secondsA-secondsB)
        stopmotor("short")
    else:
        time.sleep(secondsA)
        stopmotor("short")
        time.sleep(secondsB-secondsA)
        stopmotor("long")



def motormove(motor,direction="forward", speed = 1):
    #pin 1, the pin to move forward: pin2 is the pin that is 0
    dutycycle = convertToDutyCycle(speed)
    pin1 = False
    pin2 = False
    if (motor == "short"):
        pin1 = AIN1
        pin2 = AIN2
        
    else:
        pin1 = BIN1
        pin2 = BIN2

    if (direction == "reverse"):
        tmpPin = pin1
        pin1 = pin2
        pin2 = tmpPin

    pi.set_PWM_frequency(pin1, frequency)
    pi.set_PWM_frequency(pin2, frequency)

    pi.set_PWM_dutycycle(pin1, dutycycle)
    pi.set_PWM_dutycycle(pin2, 0)

def stopmotor(motor):
    if (motor == "short"):
        pin1 = AIN1
        pin2 = AIN2
        
    else:
        pin1 = BIN1
        pin2 = BIN2

    pi.set_PWM_dutycycle(pin2, 0)
    pi.set_PWM_dutycycle(pin1, 0)
    


