import cv2
import numpy as np
import segmentation
import time
import math
import DCMotors
import camera as cameraModule


def move_towards_point(threshold):
    currentPoint = np.array([320,240])
    movedDistance = np.array([0,0])

    diff = 999
    attempts = 0

    while(diff > threshold and attempts < 5):
        image = cameraModule.capture()
        new_image, coords, area = segmentation.combined_filter(image)

        lowestDistance = 9999.99
        lowestIndex = 999

        for i in range(len(coords)):
            coords[i][0] = 320
            tempDiff = compare_distance(currentPoint,coords[i])
            if (tempDiff < lowestDistance):
                lowestDistance = tempDiff
                lowestIndex = i

        desiredPoint = coords[lowestIndex]
        
        x_move = (desiredPoint[0] - currentPoint[0])*0.6/100
        y_move = (desiredPoint[1] - currentPoint[1])*0.6/100
        print("Moving in x...", y_move)
        x_direction = 0.5
        y_direction = 0.5

        if (x_move < 0):
            x_direction = -0.5
        if (y_move < 0):
            y_direction = -0.5

        DCMotors.motor_combined(x_direction,y_direction,abs(x_move),abs(y_move))    
        
        diff = lowestDistance
        movedDistance[0] = movedDistance[0] + x_move
        movedDistance[1] = movedDistance[1] + y_move
        attempts = attempts + 1
    return movedDistance

def compare_distance(point1, point2):
    x_diff = abs(point1[0] - point2[0])
    y_diff = abs(point1[1] - point2[1])
    return (x_diff**2 + y_diff**2)**(1/2)

        
        




