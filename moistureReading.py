import time
import math
import numpy as np
import cv2
import pigpio
#import ads1115

pi = pigpio.pi()
pin = 16
pi.set_mode(pin, pigpio.OUTPUT)
waterlevel = 0
maxWater = 12337
hiWater = 13200
lowWater = 14866
minWater = 22795

def takeMeasurement():
    waterlevel = ads1115.readRaw(config.moistureADPin, gain, sps)
    return waterlevel

waterlevel = 40

def pump(waterlevel):
    waterlevel = int(waterlevel)
    watered = False
    status = "Low"
    if (waterlevel > lowWater and waterlevel < minWater):
        pi.set_PWM_frequency(pin,50)
        pi.set_PWM_dutycycle(pin,200)
        time.sleep(2)
        pi.set_PWM_dutycycle(pin,0)
        watered = True
        status = "Low Moisture Content"
    if (waterlevel > hiWater and waterlevel < lowWater):
        pi.set_PWM_frequency(pin,50)
        pi.set_PWM_dutycycle(pin,120)
        time.sleep(2)
        pi.set_PWM_dutycycle(pin,0)
        watered = True
        status = "Medium Moisture Content"
    if (waterlevel > maxWater and waterlevel < hiWater):
        status = "High Moisture Content"
        watered = False
    

    return watered, status




