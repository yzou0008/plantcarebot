import cv2
import time

from picamera import PiCamera
from picamera.array import PiRGBArray


# initialise object
camera = PiCamera()
# configure camera setting
camera.resolution = (640,480)
camera.framerate = 20
# initialise the picture arrage with the corresponding size
rawCapture = PiRGBArray(camera, size=(640, 480))


def capture():
    framesTaken = 0

    for frame in camera.capture_continuous(rawCapture,format="bgr",use_video_port=True):
        
        # grab the raw numpy array representing the image, then initialise the timestamp and occiped/unoccupied text
        image = frame.array.copy()
        #print(image)
        rawCapture.truncate(0)
        #cv2.imshow('test',image)
        key = cv2.waitKey(1) & 0xF
        framesTaken = framesTaken + 1
        if (framesTaken > 10):
            return image
    #When everything done, release the capture


